var CACHE_VER = 'v3';

self.addEventListener('install', function (event) {
  console.log('[SW] install');
  event.waitUntil(
    caches.open(CACHE_VER)
      .then(function (cache) {
        return cache.addAll([
          './index.html',
          './js/main.js',
          './js/dataset.js',
          './css/style.css'
        ]);
      })
      .catch(function (error) {
        console.log('Error', error);
        throw error;
      })
  );
});

self.addEventListener('fetch', function(event) {
  var requestUrl = new URL(event.request.url);

  event.respondWith(
    caches.match(event.request).then(function(resp) {
      if (event.request.method !== 'GET') {
        return;
      }
      if (resp) {
        return resp;
      }
      return fetch(event.request).then(function(response) {

        if ((new RegExp('.json$')).test(requestUrl.pathname)) {
          return response;
        }

        return caches.open(CACHE_VER).then(function(cache) {
          cache.put(event.request, response.clone());
          return response;
        });
      });
    })
  );
});

self.addEventListener('message', function(event) {
  if (event.data.action === 'skipWaiting') {
    console.log(event.ports);
    event.ports[0].postMessage('Skip Waiting event');
    self.skipWaiting();
  }
});

