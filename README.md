**Service Worker learn project**
Simple service worker project.

How to run it.
```
> git clone https://gitlab.com/gribov/simple-service-worker.git
> cd server
> npm install
> node server.js
```
Then open http://localhost:8080 in browser.