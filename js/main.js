$(function ($) {

  // registerServiceWorker();
  runApp('#images');

  function runApp(rootNodeSelector) {
    var $rootNode = $(rootNodeSelector);
    createImagesBlock().then(function (block) {
      $rootNode.append(block);
    });
    
    document.getElementById('start-button').addEventListener('click', function(e) {
      e.preventDefault();
      startAnimation();
    })
  }
  var countOfCalls = 0;
  function startAnimation() {
    var el = document.querySelector('.img-thumbnail');
    var start = (new Date).getTime();
    for (var i = 1; i <= 10000; i++) {
      Math.sqrt(Math.sqrt(i));
    }
    el.setAttribute('style', 'width: ' + (el.getBoundingClientRect().width + 50) + 'px');
    console.log((new Date).getTime() - start);
    countOfCalls++;
    if (countOfCalls < 5) {
      startAnimation();
    }
  }

  function addUpdateButton(worker) {
    const button = $('<button class="btn btn-primary" id="service-worker-update">Update</button>');
    const $logList = $('#log-list');
    $logList.append(button);
    $logList.on('click', '#service-worker-update', function () {
      sendMessageToServiceWorker({action: 'skipWaiting'}, worker)
        .then(addLogMessage);
    });
  }

  function sendMessageToServiceWorker(msg, worker) {
    return new Promise(function (resolve, reject) {
      const msgChan = new MessageChannel();

      // Handler for recieving message reply from service worker
      msgChan.port1.onmessage = function (event) {
        if (event.data.error) {
          reject(event.data.error);
        } else {
          resolve(event.data);
        }
      };
      // Send message to service worker along with port for reply
      worker.postMessage(msg, [msgChan.port2]);
    });
  }

  /* Register service worker with events */
  function registerServiceWorker() {

    if (!'serviceWorker' in navigator) {
      throw new Error('serviceWorker not register');
    }

    function trackInstalling(worker) {
      worker.addEventListener('statechange', function () {
        addLogMessage('ServiceWorker state was changed to ' + worker.state);
        if (worker.state === 'installed') {
          addLogMessage('Service worker installed and waiting for activation');
          addUpdateButton(worker);
        }
      });
    }

    navigator.serviceWorker.register('/sw.js').then(function (reg) {
      if (!navigator.serviceWorker.controller) {
        return;
      }
      if (reg.active) {
        addLogMessage('There is an an active service worker');
      }

      if (reg.waiting) {
        console.log('waiting :', reg.waiting);
        addLogMessage('service worker is waiting');
        addUpdateButton(reg.waiting);
      }

      if (reg.installing) {
        console.log('installing : ', reg);
        addLogMessage('service worker status is installing');
        trackInstalling(reg.installing);
      }

      reg.addEventListener('updatefound', function () {
        addLogMessage('service worker update found');
        trackInstalling(reg.installing);
      });

      let refreshing;
      navigator.serviceWorker.addEventListener('controllerchange', function () {
        if (refreshing) return;
        setTimeout(function () {
          window.location.reload();
          refreshing = true;
        }, 1000);
      });

    }).catch(function (error) {
      console.log('Registration failed with ' + error);
    });
  }

  function createImagesBlock() {
    return fetch('images.json').then(res => res.json()).then(function (images) {
      const imagesBlock = $('<div class="images-block">');
      imagesBlock.append($('<hr>'));
      const imageBlock = $('<div class="app-image col">');

      const domImages = images.map(function (image) {
        const $domImage = $('<img class="img-thumbnail rounded">');
        $domImage.attr('src', 'loader.gif');
        fetch(image.src)
          .catch(err => {
            console.log(err);
          })
          .then(res => res.blob())
          .then(res => new Promise(resolve => {
            setTimeout(() => resolve(res), 1000);
          }))
          .then(response => {
            const url = URL.createObjectURL(response);
            // $domImage.attr('src', url);
          });
        $domImage.title = image.title;
        imageBlock.append($domImage);
        return imageBlock;
      });

      _.forEach(_.chunk(domImages, 3), function (imgGroup) {
        const groupRow = $('<div class="row">');
        _.forEach(imgGroup, function (img) {
          groupRow.append(img);
        });
        imagesBlock.append(groupRow);
      });

      return imagesBlock;
    });
  }

  function addLogMessage(message) {
    $('#log-list').append($('<li class="list-group-item">' + message + '</li>'));
  }
});
