$(function ($) {
  let lastId = null;
  const TODOS_TABLE = 'todos';
  const self = this;

  function fetchTodos(limit) {
    limit = limit || '';
    const url = 'https://jsonplaceholder.typicode.com/todos?_limit=' + limit;
    return fetch(url).then(response => response.json())
  }

  function _openDatabase(version) {
    version = version || 0;
    return new Promise((resolve, reject) => {
      if (!window.indexedDB) {
        reject();
        return;
      }
      const openRequest = window.indexedDB.open('simple_app', version);
      openRequest.onupgradeneeded = function(event) {
        const db = this.result;
        switch (event.oldVersion) {
          case 0:
            const todosStore = db.createObjectStore(TODOS_TABLE, { keyPath: 'id' });
            todosStore.createIndex('title', 'title', { unique: false });
          case 1:
            // other
        }

      };
      openRequest.onerror = reject;
      openRequest.onsuccess = function() {
        resolve(openRequest.result);
      }
    });
  }

  function clearIndexedDb() {
    return new Promise((resolve, reject) => {
      self.openDatabase().then(db => {
        const transaction = db.transaction(TODOS_TABLE, "readwrite");
        transaction.onerror = reject;
        const objectStore = transaction.objectStore(TODOS_TABLE);
        const clearRequest = objectStore.clear();
        clearRequest.onerror = reject;
        clearRequest.onsuccess = resolve;
      })
    })
  }

  function storeAllInIndexedDb(items) {
    lastId = items.reduce((res, item) => res > items.id ? res : +item.id, +items[0].id);

    return new Promise((resolve, reject) => {
      self.openDatabase().then(db => {
        const transaction = db.transaction(TODOS_TABLE, "readwrite");
        transaction.oncomplete = () => {
          resolve(items);
        };
        transaction.onerror = reject;
        const objectStore = transaction.objectStore(TODOS_TABLE);
        items.forEach(item => objectStore.put(item));
      });
    });
  }

  function storeOneInIndexedDb(item) {
    return new Promise((resolve, reject) => {
      self.openDatabase().then(db => {
        const transaction = db.transaction(TODOS_TABLE, 'readwrite');
        const objectStorte = transaction.objectStore(TODOS_TABLE);
        objectStorte.put(item);
        transaction.oncomplete = () => {
          resolve(item);
        };
        transaction.onerror = reject;
      })
    })
  }

  function fetchAllFromIndexedDb() {
    return new Promise((resolve, reject) => {
      self.openDatabase().then(db => {
        const trans = db.transaction(TODOS_TABLE, 'readonly');
        const objStore = trans.objectStore(TODOS_TABLE);
        trans.onerror = reject;
        if ('getAll' in objStore) {
          const getAllRequest = objStore.getAll();
          getAllRequest.onsuccess = (e) => {
            resolve(e.target.result);
          };
        } else {
          const items = [];
          objStore.openCursor().onsuccess = function(event) {
            const cursor = event.target.result;
            if (cursor) {
              items.push(cursor.value);
              cursor.continue();
            } else {
              resolve(items);
            }
          };
        }
      })
    })
  }
  function searchInIndexedDb(criteria) {
    criteria = criteria || {};

    return new Promise((resolve, reject) => {

      const onsuccess = function(event) {
        if (event.target.result.length) {
          resolve(event.target.result);
        } else if (event.target.result) {
          resolve([event.target.result]);
        } else {
          resolve([]);
        }
      };

      self.openDatabase().then(db => {
        const trans = db.transaction(TODOS_TABLE, 'readonly');
        const objStore = trans.objectStore(TODOS_TABLE);
        if (typeof criteria === 'function') {
          // search by custom criteria as function
          const items = [];
          return objStore.openCursor().onsuccess = function(event) {
            const cursor = event.target.result;
            if (cursor) {
              if (criteria(cursor.value)) {
                items.push(cursor.value);
              }
              cursor.continue();
            } else {
              resolve(items);
            }
          };
        }

        if (criteria.id) {
          const index = objStore.index('id').get(criteria.id);
          index.onerror = reject;
          index.onsuccess = onsuccess;
        } else if (criteria.title) {
          const index = objStore.index('title').getAll(criteria.title);
          index.onerror = reject;
          index.onsuccess = onsuccess;
        } else {
          resolve([]);
        }
      })
    })
  }

  function renderList(todos) {
    const KEYS = ['id', 'title', 'userId', 'completed', 'action'];
    const head = KEYS.reduce((res, key) => `${res}<th>${key}</th>`, '');
    const body = todos.reduce((res, item) => `${res}<tr>${KEYS.reduce((res, key) => `${res}<td>${typeof item[key] !== 'undefined' ? item[key] : ''}</td>`, '')}</tr>`, '');
    const html = `<table class="table"><thead><tr>${head}</tr></thead><tbody>${body}</tbody></table>`;
    $('#todos').html(html);
  }

  function renderOneToList(item) {
    const KEYS = ['id', 'title', 'userId', 'completed', 'action'];
    const html = `<tr class="row-added">${KEYS.reduce((res, key) => `${res}<td>${typeof item[key] !== 'undefined' ? item[key] : ''}</td>`, '')}</tr>`;
    $('#todos .table tbody').append(html);
  }

  self.openDatabase = function() {
    return _openDatabase(2);
  }

  fetchTodos(15)
    .catch(() => fetchAllFromIndexedDb())
    .then((items) => clearIndexedDb().then(() => items))
    .then(storeAllInIndexedDb)
    .then(renderList);

  document.getElementById('search-form').addEventListener('submit', function(e) {
    e.preventDefault();
    const search = this.elements['search-val'].value;
    searchInIndexedDb(item => item.title.includes(search)).then( renderList);
  });

  document.getElementById('add-form').addEventListener('submit', function(e) {
    e.preventDefault();
    const item = {
      userId: 1,
      id: lastId + 1,
      title: this.elements.title.value,
      completed: false,
    };
    storeOneInIndexedDb(item).then(item => {
      console.log('storeOneInIndexedDb', item);
      lastId = lastId + 1;
      renderOneToList(item);
      e.target.reset();
    })
  })

});

function startApp() {

}